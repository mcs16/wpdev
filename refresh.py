import argparse
import getpass
import logging
import requests
import sys
import urllib.parse

"""
This script disables and reenables all DVCS repositories on a target Jira instance.

Prerequisites:

You need the `requests` module to run this script. To install, run `python -m pip install requests`

Usage:

refresh-all-repositories.py [-h] jira_base_url username

positional arguments:
  jira_base_url  base URL for the jira instance (e.g. https://jira.my-company.com)
  username       username to log in as

Notes:

Running this script takes about 3 hours for every 10k repositories, more if there is significant network latency.
Please ensure that the box you are running this on won't go to sleep.
Results are dumped into `refresh-all-repositories.log`.

"""

GET_REPOS_PATH = "jira/rest/bitbucket/1.0/repository"
DISABLE_REPO_PATH = "jira/rest/bitbucket/1.0/repository/{}/disable"
ENABLE_REPO_PATH = "jira/rest/bitbucket/1.0/repository/{}/enable"

HEADERS = {"Accept": "application/json"}

HTTP_RATE_LIMITING_ADAPTER = requests.adapters.HTTPAdapter(max_retries=requests.packages.urllib3.util.retry.Retry(
    total=10,
    status_forcelist=[429, 502, 503, 504],
    allowed_methods=["GET", "POST"],
    backoff_factor=1
))


def get_session(jira_base_url):
    session = requests.Session()
    session.mount(jira_base_url, HTTP_RATE_LIMITING_ADAPTER)
    return session


def disable_repo(repo, jira_base_url, auth_data):
    id = repo["id"]
    path = DISABLE_REPO_PATH.format(id)
    response = get_session(jira_base_url).post(urllib.parse.urljoin(jira_base_url, path), auth=auth_data)
    if not response:
        raise RuntimeError("Unable to disable repository, HTTP status code " + str(response.status_code))


def enable_repo(repo, jira_base_url, auth_data):
    id = repo["id"]
    path = ENABLE_REPO_PATH.format(id)
    response = get_session(jira_base_url).post(urllib.parse.urljoin(jira_base_url, path), auth=auth_data)
    if not response:
        raise RuntimeError("Unable to enable repository, HTTP status code " + str(response.status_code))


def get_all_repos(jira_base_url, auth_data):
    response = get_session(jira_base_url).get(urllib.parse.urljoin(jira_base_url, GET_REPOS_PATH), auth=auth_data, headers=HEADERS)
    if response.status_code != 200:
        raise RuntimeError("Unable to retrieve repository list, HTTP status code " + str(response.status_code))
    json = response.json()
    if not "repositories" in json:
        return []
    else:
        return json["repositories"]


def dump_failure_report(repos, failures):
    logging.info("Successfully restarted " + str(len(repos) - len(failures)) + " out of " + str(len(repos)) + " repositories.")
    if len(failures) > 0:
        logging.error("Reported failures:")
        for failure in failures:
            repo = failure["repo"]
            exception = failure["exception"]
            logging.error("For repo with id = {}, url = {}".format(str(repo["id"]), repo["repositoryUrl"]))
            logging.error(exception)


if __name__ == "__main__":
    logging.basicConfig(filename='refresh-all-repositories.log', level=logging.INFO)
    logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))

    parser = argparse.ArgumentParser(description="Bulk disable and reenable DVCS repositories on a Jira instance.")
    parser.add_argument("jira_base_url", type=str, help="base URL for the jira instance (e.g. https://jira.my-company.com)")
    parser.add_argument("username", type=str, help="username to log in as")
    args = parser.parse_args()

    password = getpass.getpass()

    jira_base_url = args.jira_base_url
    auth = (args.username, password)

    repos = get_all_repos(jira_base_url, auth)

    failures = []
    counter = 1

    for repo in repos:
        try:
            if counter % 10 == 0:
                logging.info("Reenabling repository {} out of {}".format(counter, len(repos)))
            logging.debug("Reenabling repository number {} out of {}, id = {}, url = {}".format(counter, len(repos), repo["id"], repo["repositoryUrl"]))
            disable_repo(repo, jira_base_url, auth)
            enable_repo(repo, jira_base_url, auth)
            counter += 1
        except Exception as exception:
            logging.error("Failed to restart repository with id = {}".format(str(repo["id"])))
            failures.append({"repo": repo, "exception": exception})

    dump_failure_report(repos, failures)
